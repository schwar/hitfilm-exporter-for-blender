# ##### BEGIN GPL LICENSE BLOCK #####
#
# A script for Blender to export to the FXhome HitFilm composite shot format
# which is supported by HitFilm Express and HitFilm Pro.
# Copyright (C) 2019 Joshua Davies
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Export: FXhome HitFilm (.hfcs)",
    "description": "Export cameras and objects to "
        "FXhome HitFilm",
    "author": "Joshua Davies",
    "version": (0, 0, 6),
    "blender": (2, 80, 0),
    "location": "File > Export > FXhome HitFilm (.hfcs)",
    "warning": "",
    "category": "Import-Export",
}

import uuid
import bpy
import datetime
from math import degrees, floor
from mathutils import Matrix

# create list of static blender's data
def get_comp_data(context):
    scene = context.scene
    aspect_x = scene.render.pixel_aspect_x
    aspect_y = scene.render.pixel_aspect_y
    aspect = aspect_x / aspect_y
    start = scene.frame_start
    end = scene.frame_end
    active_cam_frames = get_active_cam_for_each_frame(scene, start, end)
    fps = floor(scene.render.fps / (scene.render.fps_base) * 1000.0) / 1000.0

    return {
        'scene': scene,
        'width': scene.render.resolution_x,
        'height': scene.render.resolution_y,
        'aspect': aspect,
        'fps': fps,
        'start': start,
        'end': end,
        'duration': (end - start + 1.0) / fps,
        'active_cam_frames': active_cam_frames,
        'curframe': scene.frame_current,
        }

# create list of active camera for each frame in case active camera is set by markers
def get_active_cam_for_each_frame(scene, start, end):
    active_cam_frames = []

    if not active_cam_frames:
        if scene.camera:
            # in this case active_cam_frames array will have legth of 1. This will indicate that there is only one active cam in all frames
            active_cam_frames.append(scene.camera)

    return(active_cam_frames)

# create managable list of selected objects
def get_selected(context):
    cameras = []  # list of selected cameras
    lights = []  # list of all selected lamps that can be exported as HitFilm's lights
    nulls = []  # list of all selected objects exept cameras (will be used to create nulls in HitFilm)
    obs = context.selected_objects

    for ob in obs:
        if ob.type == 'CAMERA':
            cameras.append([ob, convert_name(ob.name)])
        elif ob.type == 'LIGHT':
            lights.append([ob, ob.data.type + convert_name(ob.name)])  # Type of lamp added to name
        else:
            nulls.append([ob, convert_name(ob.name)])

    selection = {
        'cameras': cameras,
        'lights': lights,
        'nulls': nulls,
        }

    return selection

# convert names of objects to avoid errors in HitFilm.
def convert_name(name):
    name = bpy.path.clean_name(name)
    name = name.replace("-", "_")
    return name

# get object's blender's location rotation and scale and return HitFilm's Position, Rotation/Orientation and Scale
# this function will be called for every object for every frame
def convert_transform_matrix(matrix, width, height, aspect, x_rot_correction=False):
    # AE pixels to mm constant
    pixels_to_mm = 2.8352

    # get blender transform data for ob
    b_loc = matrix.to_translation()
    b_rot = matrix.to_euler('ZYX')  # ZYX euler matches HitFilm's orientation and allows to use x_rot_correction
    b_scale = matrix.to_scale()

    # convert to HitFilm Position Rotation and Scale
    x = (b_loc.x * 1000.0) / pixels_to_mm  # calculate HF's X position
    y = (b_loc.z * 1000.0) / pixels_to_mm  # calculate HF's Y position
    z = (-b_loc.y * 1000.0) / pixels_to_mm  # calculate HF's Z position

    # Convert rotations to match HitFilm's orientation.
    rx = degrees(b_rot.x) * -1.0
    ry = -degrees(b_rot.y)
    rz = -degrees(b_rot.z)

    if x_rot_correction:
        rx += 90.0  # In blender - ob of zero rotation lay on floor. In HitFilm layer of zero orientation "stands"

    # Convert scale to HitFilm scale
    sx = b_scale.x * 100.0  # scale of 1.0 is 100% in HitFilm
    sy = b_scale.z * 100.0  # scale of 1.0 is 100% in HitFilm
    sz = b_scale.y * 100.0  # scale of 1.0 is 100% in HitFilm

    return x, y, z, rx, ry, rz, sx, sy, sz

def convert_lens(camera, width, height, aspect):
    if camera.data.sensor_fit == 'VERTICAL':
        sensor = camera.data.sensor_height
        dimension = height
    else:
        sensor = camera.data.sensor_width
        dimension = width

    zoom = camera.data.lens * dimension / sensor * aspect

    return zoom

# Create a HitFilm hfcs composite shot file
def write_hfcs_file(file, comp_data, selection, include_animation, include_active_cam, include_selected_cams, include_selected_objects, scene_scalar):

    print("\n---------------------------\n- Export to HitFilm -\n---------------------------")
    # store the current frame to restore it at the end of export
    curframe = comp_data['curframe']
    # create array which will contain all keyframes values
    animation_data = {
        'time_list': [],
        'cameras': {},
        # 'lights': {},
        'nulls': {},
        }

    # create structure for active camera/cameras
    active_cam_name = ''
    if include_active_cam and comp_data['active_cam_frames'] != []:
        # check if more that one active cam exist (true if active cams set by markers)
        if len(comp_data['active_cam_frames']) is 1:
            name = convert_name(comp_data['active_cam_frames'][0].name)  # take name of the only active camera in scene
        else:
            name = 'Active_Camera'
        active_cam_name = name  # store name to be used when creating keyframes for active cam.
        animation_data['cameras'][name] = {
            'transform': '',
            'transform_list': [],
            'position_anim': False,
            'orientation_anim': False,
            'zoom': '',
            'zoom_list': [],
            'zoom_anim': False,
            }

    # create camera structure for selected cameras
    if include_selected_cams:
        for i, cam in enumerate(selection['cameras']):  # more than one camera can be selected
            if cam[1] != active_cam_name:
                name = selection['cameras'][i][1]
                animation_data['cameras'][name] = {
                    'transform': '',
                    'transform_list': [],
                    'position_anim': False,
                    'orientation_anim': False,
                    'zoom': '',
                    'zoom_list': [],
                    'zoom_anim': False,
                    }

    # create structure for nulls
    for i, obj in enumerate(selection['nulls']):  # nulls representing blender's obs except cameras, lights and solids
        if include_selected_objects:
            name = selection['nulls'][i][1]
            animation_data['nulls'][name] = {
                'transform': '',
                'transform_list': [],
                'position_anim': False,
                'orientation_anim': False,
                'scale_anim': False,
                }

    # get all keyframes for each object and store in dico
    if include_animation:
        end = comp_data['end'] + 1
    else:
        end = comp_data['start'] + 1
    for frame in range(comp_data['start'], end):
        print("working on frame: " + str(frame))
        comp_data['scene'].frame_set(frame)

        # get time for this loop
        animation_data['time_list'].append(((frame - comp_data['start']) / comp_data['fps']) * 1000)

        # keyframes for active camera/cameras
        if include_active_cam and comp_data['active_cam_frames'] != []:
            if len(comp_data['active_cam_frames']) == 1:
                cur_cam_index = 0
            else:
                cur_cam_index = frame - comp_data['start']
            active_cam = comp_data['active_cam_frames'][cur_cam_index]
            # get cam name
            name = active_cam_name
            # convert cam transform properties to HitFilm space
            transform = convert_transform_matrix(active_cam.matrix_world.copy(), comp_data['width'], comp_data['height'], comp_data['aspect'], x_rot_correction=True)
            # convert Blender's lens to HitFilm's zoom in pixels
            zoom = convert_lens(active_cam, comp_data['width'], comp_data['height'], comp_data['aspect'])

            animation_data['cameras'][name]['zoom_list'].append(zoom);
            animation_data['cameras'][name]['transform_list'].append(transform);
            # Check if properties change values compared to previous frame
            # If property don't change through out the whole animation - keyframes won't be added
            if frame != comp_data['start']:
                # get the first transform in the list
                transform_static = animation_data['cameras'][name]['transform_list'][0]
                # create our static (first) position and the current position
                position_static = [transform_static[0], transform_static[1], transform_static[2]]
                position = [transform[0], transform[1], transform[2]]
                # create our static (first) orientation and the current orientation
                orientation_static = [transform_static[3], transform_static[4], transform_static[5]]
                orientation = [transform[3], transform[4], transform[5]]
                # create our static (first) zoom and the current zoom
                zoom_static = animation_data['cameras'][name]['zoom_list'][0]
                if transform_static != position:
                    animation_data['cameras'][name]['position_anim'] = True
                if orientation_static != orientation:
                    animation_data['cameras'][name]['orientation_anim'] = True
                if zoom_static != zoom:
                    animation_data['cameras'][name]['zoom_anim'] = True

            animation_data['cameras'][name]['transform'] = transform
            animation_data['cameras'][name]['zoom'] = convert_lens(active_cam, comp_data['width'], comp_data['height'], comp_data['aspect'])

        # keyframes for selected cameras
        if include_selected_cams:
            for i, cam in enumerate(selection['cameras']):
                if cam[1] != active_cam_name:
                    # get cam name
                    name = selection['cameras'][i][1]
                    # convert cam transform properties to HitFilm space
                    transform = convert_transform_matrix(cam[0].matrix_world.copy(), comp_data['width'], comp_data['height'], comp_data['aspect'], x_rot_correction=True)
                    # convert Blender's lens to HitFilm's zoom in pixels
                    zoom = convert_lens(cam[0], comp_data['width'], comp_data['height'], comp_data['aspect'])

                    animation_data['cameras'][name]['zoom_list'].append(zoom);
                    animation_data['cameras'][name]['transform_list'].append(transform);
                    # Check if properties change values compared to previous frame
                    # If property don't change through out the whole animation - keyframes won't be added
                    if frame != comp_data['start']:
                        # get the first transform in the list
                        transform_static = animation_data['cameras'][name]['transform_list'][0]
                        # create our static (first) position and the current position
                        position_static = [transform_static[0], transform_static[1], transform_static[2]]
                        position = [transform[0], transform[1], transform[2]]
                        # create our static (first) orientation and the current orientation
                        orientation_static = [transform_static[3], transform_static[4], transform_static[5]]
                        orientation = [transform[3], transform[4], transform[5]]
                        # create our static (first) zoom and the current zoom
                        zoom_static = animation_data['cameras'][name]['zoom_list'][0]
                        if transform_static != position:
                            animation_data['cameras'][name]['position_anim'] = True
                        if orientation_static != orientation:
                            animation_data['cameras'][name]['orientation_anim'] = True
                        if zoom_static != zoom:
                            animation_data['cameras'][name]['zoom_anim'] = True

                    animation_data['cameras'][name]['transform'] = transform
                    animation_data['cameras'][name]['zoom'] = convert_lens(cam[0], comp_data['width'], comp_data['height'], comp_data['aspect'])


        # keyframes for all nulls
        if include_selected_objects:
            for i, ob in enumerate(selection['nulls']):
                # get object name
                name = selection['nulls'][i][1]
                # convert ob transform properties to HitFilm space
                transform = convert_transform_matrix(ob[0].matrix_world.copy(), comp_data['width'], comp_data['height'], comp_data['aspect'], x_rot_correction=True)

                animation_data['nulls'][name]['transform_list'].append(transform);
                # Check if properties change values compared to previous frame
                # If property don't change through out the whole animation - keyframes won't be added
                if frame != comp_data['start']:
                    # get the first transform in the list
                    transform_static = animation_data['nulls'][name]['transform_list'][0]
                    # create our static (first) position and the current position
                    position_static = [transform_static[0], transform_static[1], transform_static[2]]
                    position = [transform[0], transform[1], transform[2]]
                    # create our static (first) orientation and the current orientation
                    orientation_static = [transform_static[3], transform_static[4], transform_static[5]]
                    orientation = [transform[3], transform[4], transform[5]]
                    # create our static (first) scale and the current scale
                    scale_static = [transform_static[6], transform_static[7], transform_static[8]]
                    scale = [transform[6], transform[7], transform[8]]

                    if transform_static != position:
                        animation_data['nulls'][name]['position_anim'] = True
                    if orientation_static != orientation:
                        animation_data['nulls'][name]['orientation_anim'] = True
                    if scale_static != scale:
                        animation_data['nulls'][name]['scale_anim'] = True

                animation_data['nulls'][name]['transform'] = transform

    # ---- write hfcs file
    hfcs_file = open(file, 'w')

    # create HitFilm composite shot
    hfcs_file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
    hfcs_file.write('<BiffCompositeShot>\n')
    hfcs_file.write('\t<CompositionAsset Version="1">\n')
    # create uid for this composite shot
    comp_uuid = str(uuid.uuid4())
    hfcs_file.write('\t\t<ID>%s</ID>\n' % comp_uuid)
    # set the name of the composite shot to that of the blender scene
    hfcs_file.write('\t\t<Name>%s</Name>\n' % comp_data['scene'].name)
    hfcs_file.write('\t\t<ParentFolderID>00000000-0000-0000-0000-000000000000</ParentFolderID>\n')
    hfcs_file.write('\t\t<CTI>0</CTI>\n')
    hfcs_file.write('\t\t<InPoint>0</InPoint>\n')
     # set the duration to the length of the blender scene
    hfcs_file.write('\t\t<OutPoint>%i</OutPoint>\n' % (comp_data['duration'] * comp_data['fps']))
    hfcs_file.write('\t\t<TimelineZoom>0</TimelineZoom>\n')
    hfcs_file.write('\t\t<SplitterPosition>440</SplitterPosition>\n')
    hfcs_file.write('\t\t<TimelineTimeFormat>1000</TimelineTimeFormat>\n')
    hfcs_file.write('\t\t<TimelineSnapMode>1000</TimelineSnapMode>\n')
    hfcs_file.write('\t\t<AudioVideoSettings Version="0">\n')
     # set the duration to the length of the blender scene
    hfcs_file.write('\t\t\t<FrameCount>%i</FrameCount>\n' % (comp_data['duration'] * comp_data['fps']))
    hfcs_file.write('\t\t\t<AudioSampleRate>48000</AudioSampleRate>\n')
     # set the width and height to that of the blender scene
    hfcs_file.write('\t\t\t<Width>%i</Width>\n' % comp_data['width'])
    hfcs_file.write('\t\t\t<Height>%i</Height>\n' % comp_data['height'])
    hfcs_file.write('\t\t\t<Fields>0</Fields>\n')
    hfcs_file.write('\t\t\t<PAR>0</PAR>\n')
    hfcs_file.write('\t\t\t<PARCustom>1</PARCustom>\n')
     # set the fps to that of the blender scene
    hfcs_file.write('\t\t\t<FrameRate>%f</FrameRate>\n' % comp_data['fps'])
    hfcs_file.write('\t\t</AudioVideoSettings>\n')
    hfcs_file.write('\t\t<RenderSettings Version="1">\n')
    hfcs_file.write('\t\t\t<FogEnabled>0</FogEnabled>\n')
    hfcs_file.write('\t\t\t<FogNearDistance>900</FogNearDistance>\n')
    hfcs_file.write('\t\t\t<FogFarDistance>2000</FogFarDistance>\n')
    hfcs_file.write('\t\t\t<FogDensity>1</FogDensity>\n')
    hfcs_file.write('\t\t\t<FogColor G="0" R="0" A="1" B="0"/>\n')
    hfcs_file.write('\t\t\t<FogFalloff>0</FogFalloff>\n')
    hfcs_file.write('\t\t\t<MotionBlurEnabled>1</MotionBlurEnabled>\n')
    hfcs_file.write('\t\t\t<ShutterAngle>180</ShutterAngle>\n')
    hfcs_file.write('\t\t\t<ShutterPhase>-90</ShutterPhase>\n')
    hfcs_file.write('\t\t\t<MaxNumOfSamples>20</MaxNumOfSamples>\n')
    hfcs_file.write('\t\t\t<UseAdaptiveSamples>1</UseAdaptiveSamples>\n')
    hfcs_file.write('\t\t</RenderSettings>\n')
    hfcs_file.write('\t\t<Layers>\n')

    # create cameras
    for i, cam in enumerate(animation_data['cameras']):  # more than one camera can be selected
        name = cam
        hfcs_file.write('\t\t\t<CameraLayer Version="1">\n')
        hfcs_file.write('\t\t\t\t<Dimensions>1</Dimensions>\n')
        hfcs_file.write('\t\t\t\t<HasDoF>0</HasDoF>\n')
        hfcs_file.write('\t\t\t\t<AutoOrient>0</AutoOrient>\n')
        hfcs_file.write('\t\t\t\t<AutoOrientPathAxis>3</AutoOrientPathAxis>\n')
        hfcs_file.write('\t\t\t\t<NearClip>0.100000001</NearClip>\n')
        hfcs_file.write('\t\t\t\t<FarClip>100000</FarClip>\n')
        hfcs_file.write('\t\t\t\t<LayerBase Version="0">\n')
        # create uid for this camera shot
        cam_uuid = str(uuid.uuid4())
        hfcs_file.write('\t\t\t\t\t<ID>%s</ID>\n' % cam_uuid)
        # set the name of the camera
        hfcs_file.write('\t\t\t\t\t<Name>%s</Name>\n' % name)
        # set the uid of the composite shot which owns the camera
        hfcs_file.write('\t\t\t\t\t<CompID>%s</CompID>\n' % comp_uuid)
        hfcs_file.write('\t\t\t\t\t<ParentLayerID>00000000-0000-0000-0000-000000000000</ParentLayerID>\n')
        hfcs_file.write('\t\t\t\t\t<StartFrame>0</StartFrame>\n')
         # set the duration to the length of the blender scene
        hfcs_file.write('\t\t\t\t\t<EndFrame>%i</EndFrame>\n' % (comp_data['duration'] * comp_data['fps']))
        hfcs_file.write('\t\t\t\t\t<BlendMode>0</BlendMode>\n')
        hfcs_file.write('\t\t\t\t\t<Visible>1</Visible>\n')
        hfcs_file.write('\t\t\t\t\t<Muted>0</Muted>\n')
        hfcs_file.write('\t\t\t\t\t<Locked>0</Locked>\n')
        hfcs_file.write('\t\t\t\t\t<MotionBlurOn>0</MotionBlurOn>\n')
        # add a property manager to hold our property data
        hfcs_file.write('\t\t\t\t\t<PropertyManager Version="0">\n')
        # add position data
        hfcs_file.write('\t\t\t\t\t\t<position Type="0" Spatial="1">\n')
        # get the transform of the camera
        transform = animation_data['cameras'][cam]['transform']
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="%f" Y="%f" Z="%f"/>\n' % (transform[0] * scene_scalar, transform[1] * scene_scalar, transform[2] * scene_scalar))
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="%f" Y="%f" Z="%f"/>\n' % (transform[0] * scene_scalar, transform[1] * scene_scalar, transform[2] * scene_scalar))
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        # add position animation
        if include_animation and animation_data['cameras'][name]['position_anim']:
            hfcs_file.write('\t\t\t\t\t\t\t<Animation>\n')
            for f, transform_item in enumerate(animation_data['cameras'][cam]['transform_list']):
                time_item = animation_data['time_list'][f]
                hfcs_file.write('\t\t\t\t\t\t\t\t<Key Type="1" Time="%i">\n' % time_item)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t<Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t\t\t<FXPoint3_32f X="%f" Y="%f" Z="%f"/>\n' % (transform_item[0] * scene_scalar, transform_item[1] * scene_scalar, transform_item[2] * scene_scalar))
                hfcs_file.write('\t\t\t\t\t\t\t\t\t</Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t</Key>\n')
            hfcs_file.write('\t\t\t\t\t\t\t</Animation>\n')
        hfcs_file.write('\t\t\t\t\t\t</position>\n')
        # add target data
        hfcs_file.write('\t\t\t\t\t\t<target Type="0" Spatial="1">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="0" Y="0" Z="0"/>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="0" Y="0" Z="0"/>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</target>\n')
        # add orientation data
        hfcs_file.write('\t\t\t\t\t\t<orientation Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<Orientation3D X="%f" Y="%f" Z="%f"/>\n' % (transform[3], transform[4], transform[5]))
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<Orientation3D X="%f" Y="%f" Z="%f"/>\n' % (transform[3], transform[4], transform[5]))
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        # add orientation animation
        if include_animation and animation_data['cameras'][name]['orientation_anim']:
            hfcs_file.write('\t\t\t\t\t\t\t<Animation>\n')
            for o, transform_item in enumerate(animation_data['cameras'][cam]['transform_list']):
                time_item = animation_data['time_list'][o]
                hfcs_file.write('\t\t\t\t\t\t\t\t<Key Type="1" Time="%i">\n' % time_item)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t<Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t\t\t<Orientation3D X="%f" Y="%f" Z="%f"/>\n' % (transform_item[3], transform_item[4], transform_item[5]))
                hfcs_file.write('\t\t\t\t\t\t\t\t\t</Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t</Key>\n')
            hfcs_file.write('\t\t\t\t\t\t\t</Animation>\n')
        hfcs_file.write('\t\t\t\t\t\t</orientation>\n')
        # add rotation data
        hfcs_file.write('\t\t\t\t\t\t<rotationX Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</rotationX>\n')
        hfcs_file.write('\t\t\t\t\t\t<rotationY Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</rotationY>\n')
        hfcs_file.write('\t\t\t\t\t\t<rotationZ Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</rotationZ>\n')
        # add aperture data
        hfcs_file.write('\t\t\t\t\t\t<aperture Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>17.7199993</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>17.7199993</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</aperture>\n')
        # add zoom data
        write_zoom = animation_data['cameras'][cam]['zoom']
        hfcs_file.write('\t\t\t\t\t\t<zoom Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>%f</float>\n' % write_zoom)
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>%f</float>\n' % write_zoom)
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        # add zoom animation
        if include_animation and animation_data['cameras'][name]['zoom_anim']:
            hfcs_file.write('\t\t\t\t\t\t\t<Animation>\n')
            for z, ae_keyframe_zoom in enumerate(animation_data['cameras'][cam]['zoom_list']):
                time_item = animation_data['time_list'][z]
                hfcs_file.write('\t\t\t\t\t\t\t\t<Key Type="1" Time="%i">\n' % time_item)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t<Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t\t\t<float>%f</float>\n' % ae_keyframe_zoom)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t</Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t</Key>\n')
            hfcs_file.write('\t\t\t\t\t\t\t</Animation>\n')
        hfcs_file.write('\t\t\t\t\t\t</zoom>\n')
        # add focus data
        hfcs_file.write('\t\t\t\t\t\t<focusDistance Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>%f</float>\n' % write_zoom)
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>%f</float>\n' % write_zoom)
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</focusDistance>\n')
        # add depth-of-field data
        hfcs_file.write('\t\t\t\t\t\t<blurDoF Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>100</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>100</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</blurDoF>\n')
        hfcs_file.write('\t\t\t\t\t</PropertyManager>\n')
        hfcs_file.write('\t\t\t\t</LayerBase>\n')
        hfcs_file.write('\t\t\t</CameraLayer>\n')

    # create objects (nulls)
    for i, obj in enumerate(animation_data['nulls']):
        name = obj

        hfcs_file.write('\t\t\t<PointLayer Version="0">\n')
        hfcs_file.write('\t\t\t\t<Dimensions>1</Dimensions>\n')
        hfcs_file.write('\t\t\t\t<LayerBase Version="0">\n')
        # create uid for this null
        point_uuid = str(uuid.uuid4())
        hfcs_file.write('\t\t\t\t\t<ID>%s</ID>\n' % point_uuid)
        # set the name of the null
        hfcs_file.write('\t\t\t\t\t<Name>%s</Name>\n' % name)
        # set the uid of the composite shot which owns the null
        hfcs_file.write('\t\t\t\t\t<CompID>%s</CompID>\n' % comp_uuid)
        hfcs_file.write('\t\t\t\t\t<ParentLayerID>00000000-0000-0000-0000-000000000000</ParentLayerID>\n')
        hfcs_file.write('\t\t\t\t\t<StartFrame>0</StartFrame>\n')
         # set the duration to the length of the blender scene
        hfcs_file.write('\t\t\t\t\t<EndFrame>%i</EndFrame>\n' % (comp_data['duration'] * comp_data['fps']))
        hfcs_file.write('\t\t\t\t\t<BlendMode>0</BlendMode>\n')
        hfcs_file.write('\t\t\t\t\t<Visible>1</Visible>\n')
        hfcs_file.write('\t\t\t\t\t<Muted>0</Muted>\n')
        hfcs_file.write('\t\t\t\t\t<Locked>0</Locked>\n')
        hfcs_file.write('\t\t\t\t\t<MotionBlurOn>0</MotionBlurOn>\n')
        # add a property manager to hold our property data
        hfcs_file.write('\t\t\t\t\t<PropertyManager Version="0">\n')
        # add anchor data
        hfcs_file.write('\t\t\t\t\t\t<anchorPoint Type="0" Spatial="1">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="0" Y="0" Z="0"/>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="0" Y="0" Z="0"/>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</anchorPoint>\n')
        # add position data
        hfcs_file.write('\t\t\t\t\t\t<position Type="0" Spatial="1">\n')
        # get the transform of the null
        transform = animation_data['nulls'][obj]['transform']
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="%f" Y="%f" Z="%f"/>\n' % (transform[0] * scene_scalar, transform[1] * scene_scalar, transform[2] * scene_scalar))
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<FXPoint3_32f X="%f" Y="%f" Z="%f"/>\n' % (transform[0] * scene_scalar, transform[1] * scene_scalar, transform[2] * scene_scalar))
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        # add position animation
        if include_animation and animation_data['nulls'][name]['position_anim']:
            hfcs_file.write('\t\t\t\t\t\t\t<Animation>\n')
            for f, transform_item in enumerate(animation_data['nulls'][name]['transform_list']):
                time_item = animation_data['time_list'][f]
                hfcs_file.write('\t\t\t\t\t\t\t\t<Key Type="1" Time="%i">\n' % time_item)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t<Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t\t\t<FXPoint3_32f X="%f" Y="%f" Z="%f"/>\n' % (transform_item[0] * scene_scalar, transform_item[1] * scene_scalar, transform_item[2] * scene_scalar))
                hfcs_file.write('\t\t\t\t\t\t\t\t\t</Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t</Key>\n')
            hfcs_file.write('\t\t\t\t\t\t\t</Animation>\n')
        hfcs_file.write('\t\t\t\t\t\t</position>\n')
        # add orientation data
        hfcs_file.write('\t\t\t\t\t\t<orientation Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<Orientation3D X="%f" Y="%f" Z="%f"/>\n' % (transform[3], transform[4], transform[5]))
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<Orientation3D X="%f" Y="%f" Z="%f"/>\n' % (transform[3], transform[4], transform[5]))
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        # add orientation animation
        if include_animation and animation_data['nulls'][name]['orientation_anim']:
            hfcs_file.write('\t\t\t\t\t\t\t<Animation>\n')
            for o, transform_item in enumerate(animation_data['nulls'][name]['transform_list']):  # more than one camera can be selected
                time_item = animation_data['time_list'][o]
                hfcs_file.write('\t\t\t\t\t\t\t\t<Key Type="1" Time="%i">\n' % time_item)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t<Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t\t\t<Orientation3D X="%f" Y="%f" Z="%f"/>\n' % (transform_item[3], transform_item[4], transform_item[5]))
                hfcs_file.write('\t\t\t\t\t\t\t\t\t</Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t</Key>\n')
            hfcs_file.write('\t\t\t\t\t\t\t</Animation>\n')
        hfcs_file.write('\t\t\t\t\t\t</orientation>\n')
        # add scale data
        hfcs_file.write('\t\t\t\t\t\t<scale Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<Scale3D X="%f" Y="%f" Z="%f"/>\n' % (transform[6] * scene_scalar, transform[7] * scene_scalar, transform[8] * scene_scalar))
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<Scale3D X="%f" Y="%f" Z="%f"/>\n' % (transform[6] * scene_scalar, transform[7] * scene_scalar, transform[8] * scene_scalar))
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        # add scale animation
        if include_animation and animation_data['nulls'][name]['scale_anim']:
            hfcs_file.write('\t\t\t\t\t\t\t<Animation>\n')
            for o, transform_item in enumerate(animation_data['nulls'][name]['transform_list']):
                time_item = animation_data['time_list'][o]
                hfcs_file.write('\t\t\t\t\t\t\t\t<Key Type="1" Time="%i">\n' % time_item)
                hfcs_file.write('\t\t\t\t\t\t\t\t\t<Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t\t\t<Scale3D X="%f" Y="%f" Z="%f"/>\n' % (transform_item[6] * scene_scalar, transform_item[7] * scene_scalar, transform_item[8] * scene_scalar))
                hfcs_file.write('\t\t\t\t\t\t\t\t\t</Value>\n')
                hfcs_file.write('\t\t\t\t\t\t\t\t</Key>\n')
            hfcs_file.write('\t\t\t\t\t\t\t</Animation>\n')
        hfcs_file.write('\t\t\t\t\t\t</scale>\n')
        # add scale linked data
        hfcs_file.write('\t\t\t\t\t\t<scaleLinked Type="1" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<bool>1</bool>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<bool>1</bool>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</scaleLinked>\n')
        # add rotation data
        hfcs_file.write('\t\t\t\t\t\t<rotationX Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</rotationX>\n')
        hfcs_file.write('\t\t\t\t\t\t<rotationY Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</rotationY>\n')
        hfcs_file.write('\t\t\t\t\t\t<rotationZ Type="0" Spatial="0">\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Default>\n')
        hfcs_file.write('\t\t\t\t\t\t\t<Static>\n')
        hfcs_file.write('\t\t\t\t\t\t\t\t<float>0</float>\n')
        hfcs_file.write('\t\t\t\t\t\t\t</Static>\n')
        hfcs_file.write('\t\t\t\t\t\t</rotationZ>\n')
        hfcs_file.write('\t\t\t\t\t</PropertyManager>\n')
        hfcs_file.write('\t\t\t\t</LayerBase>\n')
        hfcs_file.write('\t\t\t</PointLayer>\n')

    hfcs_file.write('\t\t</Layers>\n')
    hfcs_file.write('\t<Instances/>\n')
    hfcs_file.write('\t</CompositionAsset>\n')
    hfcs_file.write('</BiffCompositeShot>\n')

    hfcs_file.close()

    comp_data['scene'].frame_set(curframe)  # set current frame of animation in blender to state before export

##########################################
# DO IT
##########################################

def main(file, context, include_animation, include_active_cam, include_selected_cams, include_selected_objects, scene_scalar):
    comp_data = get_comp_data(context)
    selection = get_selected(context)
    write_hfcs_file(file, comp_data, selection, include_animation, include_active_cam, include_selected_cams, include_selected_objects, scene_scalar)
    print ("\nExport to HitFilm Completed")
    return {'FINISHED'}

##########################################
# ExportHfcs class register/unregister
##########################################

from bpy_extras.io_utils import ExportHelper
from bpy.props import StringProperty, BoolProperty, FloatProperty

class ExportHfcs(bpy.types.Operator, ExportHelper):
    """Export selected cameras and objects animation to HitFilm"""
    bl_idname = "export.hfcs"
    bl_label = "Export to FXhome HitFilm"
    filename_ext = ".hfcs"
    filter_glob: StringProperty(default="*.hfcs", options={'HIDDEN'})

    include_animation: BoolProperty(
            name="Animation",
            description="Animate Exported Cameras and Objects",
            default=True,
            )
    include_active_cam: BoolProperty(
            name="Active Camera",
            description="Include Active Camera",
            default=True,
            )
    include_selected_cams: BoolProperty(
            name="Selected Cameras",
            description="Add Selected Cameras",
            default=True,
            )
    include_selected_objects: BoolProperty(
            name="Selected Objects",
            description="Export Selected Objects",
            default=True,
            )
    scene_scalar: FloatProperty(
            name="Scalar",
            description="Applies Scalar to Export",
            default=1.0,
            min=0.01,
            max=100.0,
            )

    def draw(self, context):
        layout = self.layout

        box = layout.box()
        box.label(text='Animation:')
        box.prop(self, 'include_animation')
        box.label(text='Include Cameras and Objects:')
        box.prop(self, 'include_active_cam')
        box.prop(self, 'include_selected_cams')
        box.prop(self, 'include_selected_objects')
        box.label(text="Scene scalar:")
        box.prop(self, 'scene_scalar')

    @classmethod
    def poll(cls, context):
        active = context.active_object
        selected = context.selected_objects
        camera = context.scene.camera
        ok = selected or camera
        return ok

    def execute(self, context):
        return main(self.filepath, context, self.include_animation, self.include_active_cam, self.include_selected_cams, self.include_selected_objects, self.scene_scalar)

def menu_func(self, context):
    self.layout.operator(ExportHfcs.bl_idname, text="FXhome HitFilm (.hfcs)")

def register():
    bpy.utils.register_class(ExportHfcs)
    bpy.types.TOPBAR_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_class(ExportHfcs)
    bpy.types.TOPBAR_MT_file_export.remove(menu_func)

if __name__ == "__main__":
    register()
